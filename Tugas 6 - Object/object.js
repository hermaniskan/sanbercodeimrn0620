//No.1 - Array to Object
function arrayToObject(arr) {
    // Code di sini 
    var peoples  = {};
    var date_now = new Date();
    var year_now = date_now.getFullYear();
    for(var i = 0; i < arr.length; i++){
        var data = {}        
        var full_name = ""
        full_name += arr[i][0] + " " + arr[i][1]
        data.firstName = arr[i][0]
        data.lastName  = arr[i][1]
        data.gender    = arr[i][2]
        var age        = arr[i][3]
        if(age < year_now){
            data.age = year_now - arr[i][3]
        }else{
            data.age = "Invalid birth year"
        } 

        peoples[full_name] = data
    }
    console.log(peoples);
}
 
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
arrayToObject([]) // ""



//No.2 Shopping Time
function shoppingTime(memberId, money) {
    // you can only write your code here!
    var produk = [ ['Sepatu Stacattu', 1500000], ['Baju Zoro', 500000], ['Baju H&N', 250000], ['Sweater Uniklooh', 175000], ['Casing Handphone', 50000]]
    var all_data = {}
    all_data.memberId = memberId
    all_data.money    = money
    var list_purchased = []
    if(memberId != '' && money >= 50000){
        for(i = 0; i < produk.length; i++){
            var kembalian      = 0
            if(money >= produk[i][1]){
                list_purchased.push(produk[i][0])
                money -= produk[i][1] 
                // kembalian = money - produk[i][1]
            }
        }
        all_data.listPurchased = list_purchased
        all_data.changeMoney   = money
    
        return all_data;
    }else if(memberId != '' && money < 50000){
        return "Mohon maaf, Uang tidak cukup"
    }else{
        return "Mohon maaf, toko X hanya berlaku untuk member"
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja



  //No.3 - Naik Angkot
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    all_data = []
    //your code here
    for(i = 0; i < arrPenumpang.length; i++){
        data = {}
        tarif = 0
        data.penumpang = arrPenumpang[i][0]
        data.naikDari  = arrPenumpang[i][1]
        data.tujuan    = arrPenumpang[i][2]
        for(x = 0; x < rute.length; x++){
            awal  = 0
            akhir = 0
            jum_rute = 0
            if(data.naikDari == rute[x]){
                awal = x
            }else if(data.tujuan == rute[x]){
                akhir = x
            }
            jum_rute = akhir - awal
            tarif    += jum_rute * 2000
        }
        data.bayar = Math.abs(tarif)
        all_data.push(data)
    }
    return all_data;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]