import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Text, FlatList } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

//Import for Content
import VideoItem from './components/videoItem';
import data from './data.json';

export default class App extends React.Component {
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image source={require('./images/logo.png')} style={styles.logo}/>
          <View style={styles.rightNav}>
            <TouchableOpacity>  
              <Icon style={styles.navItem} name="search" size={25} />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon style={styles.navItem} name="account-circle" size={25} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.content}>
          <FlatList
          data={data.items}
          renderItem={(video) => <VideoItem video={video.item} />}
          ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
          />
        </View>
        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="home" size={25}/>
            <Text style={styles.tabTittle}>
              Home
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="whatshot" size={25}/>
            <Text style={styles.tabTittle}>
              Trending
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="subscriptions" size={25}/>
            <Text style={styles.tabTittle}>
              Subscriptions
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="folder" size={25}/>
            <Text style={styles.tabTittle}>
              Library
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    flex : 1
  },
  navBar : {
    height           : 70,
    backgroundColor  : 'white',
    elevation        : 3,
    flexDirection    : 'row',
    paddingHorizontal: 15,
    alignItems       : 'center',
    justifyContent : 'space-between'
  },
  logo : {
    height : 22,
    width : 99
  },
  rightNav : {
    flexDirection : 'row'
  },
  navItem : {
    marginLeft : 25
  },
  content : {
    flex : 1
  },
  tabBar : {
    height : 60,
    backgroundColor : 'white',
    borderTopWidth : 0.5,
    borderColor : '#E5E5E5',
    flexDirection : 'row',
    justifyContent :  'space-around'
  },
  tabItem : {
    alignItems : 'center',
    justifyContent : 'center'
  },
  tabTittle : {
    fontSize : 11,
    color : '#3c3c3c'
  },

  itemSeparator : {
    height : 0.5,
    backgroundColor : '#cccccc'
  }
});

