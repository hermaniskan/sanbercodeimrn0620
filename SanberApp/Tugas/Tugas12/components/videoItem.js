import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Text } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';


export default class VideoItem extends React.Component {
  render(){
    let video = this.props.video;
    return(
      <View style={styles.container}>
        <Image source={{uri:video.snippet.thumbnails.medium.url}} style={styles.videoImage} />
        <View style={styles.descContiner}>
          <Image source={{uri:'https://randomuser.me/api/portraits/men/45.jpg'}} style={styles.imgChannel} />
          <View style={styles.videoDetails}>
            <Text numberOfLines={2} style={styles.videoTitle}>{video.snippet.title}</Text>
            <Text style={styles.videoStats}>{video.snippet.channelTitle + " · " + nFormatter(video.statistics.viewCount, 1) + ' · 3days ago'}</Text>
          </View>
          <TouchableOpacity>
            <Icon name="more-vert" size={20} color="#888888" />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

function nFormatter(num, digits) {
  var si = [
    { value: 1, symbol: "" },
    { value: 1E3, symbol: "k" },
    { value: 1E6, symbol: "M" },
    { value: 1E9, symbol: "G" },
    { value: 1E12, symbol: "T" },
    { value: 1E15, symbol: "P" },
    { value: 1E18, symbol: "E" }
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + " views";
}

const styles = StyleSheet.create({
  container : {
    padding : 15
  },
  videoImage : {
    height : 200
  },
  descContiner : {
    flexDirection : 'row',
    paddingTop : 15
  },
  imgChannel : {
    height : 50,
    width : 50,
    borderRadius : 25
  },
  videoDetails : {
    paddingHorizontal : 15,
    flex : 1
  },
  videoTitle : {
    fontSize : 17,
    color : '#3c3c3c'
  },
  videoStats : { 
    fontSize : 15,
    paddingTop : 3
  }
});