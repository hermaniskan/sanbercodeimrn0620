import React from 'react';

import { StyleSheet, View, Image, Text, TextInput, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import { FontAwesome } from '@expo/vector-icons'; 



export default class AboutScreen extends React.Component {
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.profileContainer}>
          <Text style={styles.textTitle}>Tentang Saya</Text>
          <View style={styles.avatarContainer}>
            <Icon name="person" size={180} style={styles.avatarIcon}/>
          </View>
          <View style={styles.profileDesc}>
            <Text style={styles.textName}>Herman Iskandar</Text>
            <Text style={styles.textJob}>Full Stack Developer</Text>
          </View>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.detailTitle}>Portofolio</Text>
          <View style={styles.detailItemPortofolio}>
            <TouchableOpacity style={styles.portofolioItem}>
              <FontAwesome name="gitlab" size={80} color="#3EC6FF" />
              <Text style={styles.textAcount}>@hermaniskandar</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.portofolioItem}>
              <FontAwesome name="github" size={80} color="#3EC6FF" />
              <Text style={styles.textAcount}>@hermaniskandar</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.detailTitle}>Hubungi Saya</Text>
          <View style={styles.detailItemContact}>
            <TouchableOpacity style={styles.contactItem}>
              <FontAwesome name="facebook-square" size={40} color="#3EC6FF" />
              <Text style={styles.textAcountContact}>herman iskandar</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.contactItem}>
              <FontAwesome name="instagram" size={40} color="#3EC6FF" />
              <Text style={styles.textAcountContact}>@hermaniskandar</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.contactItem}>
              <FontAwesome name="twitter" size={40} color="#3EC6FF" />
              <Text style={styles.textAcountContact}>@hermaniskandar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    flex : 1,
    padding : 15,
  },
  profileContainer : {
    alignItems : 'center',
    height : 330,
    marginTop : 30,
    justifyContent : 'space-around'
  },
  textTitle : {
    fontSize : 30,
    fontWeight : 'bold'
  },
  avatarContainer : {
    width : 180,
    height : 180,
    borderRadius : 100,
    backgroundColor : '#EFEFEF'
  },
  avatarIcon : {
    color : '#cfbebe'
  },
  profileDesc : {
    alignItems : 'center'
  },
  textName : {
    fontSize : 25
  },
  textJob : {
    fontSize : 17,
    color : '#3EC6FF'
  },
  detailContainer : {
    // height : 180,
    borderRadius : 20,
    marginTop : 20,
    backgroundColor : '#EFEFEF',
    padding : 10
  },
  detailTitle : {
    fontSize : 18,
  },
  detailItemPortofolio : {
    borderTopWidth : 1,
    flexDirection : 'row',
    padding : 20,
    justifyContent : 'space-around',
    alignItems : 'center'
  },
  portofolioItem : {
    alignItems : 'center'
  },
  detailItemContact : {
    borderTopWidth : 1,
    justifyContent : 'space-around'
  },
  contactItem : {
    flexDirection : 'row',
    alignItems : 'center',
    justifyContent : 'center',
    padding : 5
  },
  textAcount : {
    color : '#003366',
    fontSize : 18,
    fontWeight : 'bold'
  },
  textAcountContact : {
    color : '#003366',
    fontSize : 18,
    fontWeight : 'bold',
    paddingLeft : 10
  }
});