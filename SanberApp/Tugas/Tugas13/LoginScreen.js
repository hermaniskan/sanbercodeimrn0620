import React from 'react';

import { StyleSheet, View, Image, Text, TextInput, TouchableOpacity } from 'react-native';

export default class LoginScreen extends React.Component {
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={require('./images/logo.png')} style={styles.logo} />
        </View>
        <View style={styles.formContainer}>
          <Text style={styles.formTitle}>Login</Text>
          <View style={styles.formField}>
            <Text style={styles.fieldLabel}>Username / Email</Text>
            <TextInput
            style={styles.fieldInput}
            />
          </View>
          <View style={styles.formField}>
            <Text style={styles.fieldLabel}>Password</Text>
            <TextInput
            style={styles.fieldInput}
            />
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.buttonLogin}>
            <Text style={styles.textButton}>Masuk</Text>
          </TouchableOpacity>
          <Text style={styles.textOr}>atau</Text>
          <TouchableOpacity style={styles.buttonRegister}>
            <Text style={styles.textButton}>Daftar ?</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    backgroundColor: 'white',
    padding        : 15,
    flex           : 1,
  },
  logoContainer : {
    paddingTop : 100,
  },
  logo : {

  },
  formContainer : {
    height     : 300,
  },
  formTitle : {
    fontSize  : 22,
    fontWeight: 'bold',
    paddingTop: 50,
    alignSelf : 'center',
    paddingBottom : 10
  },
  formField : {
    paddingTop: 15,
    width     : 350,
    alignSelf : 'center'
  },
  fieldLabel : {
    fontSize     : 15,
    paddingBottom: 5,
  },
  fieldInput : {
    height     : 50,
    borderWidth: 1,
    borderColor: '#003366',
  },
  buttonContainer : {
    height : 150,
    paddingTop : 20,
    alignItems : 'center',
    justifyContent : 'space-between'
  },
  buttonLogin : {
    width : 130,
    height : 40,
    borderRadius : 18,
    backgroundColor : '#3EC6FF',
    alignItems : 'center',
    justifyContent : 'center'
  },
  textOr : {
    fontSize : 22,
    color : '#3EC6FF'
  },
  textButton : {
    color : 'white',
    fontSize : 22
  },
  buttonRegister : {
    width : 130,
    height : 40,
    borderRadius : 18,
    backgroundColor : '#003366',
    alignItems : 'center',
    justifyContent : 'center'
  }
});