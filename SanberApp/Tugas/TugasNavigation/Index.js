import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import SkillScreen from './SkillScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';


const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator
    screenOptions={{
      headerShown: false
    }}
  >
    <AuthStack.Screen
      name      = "Login"
      component = {LoginScreen}
    />
    <AuthStack.Screen
      name      = "AboutScreen"
      component = {DrawerScreen}
    />

  </AuthStack.Navigator>
)

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen
      name="Skill" 
      component={SkillScreen}
    />
    <Tabs.Screen
      name="Project" 
      component={ProjectScreen}
    />
    <Tabs.Screen
      name="Add" 
      component={AddScreen}
    />
  </Tabs.Navigator>
)

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen 
      name="About"
      component={AboutScreen}
    />
    <Drawer.Screen 
      name="Skill"
      component={TabsScreen}
    />
  </Drawer.Navigator>
)


export default() => {
  return(
    <NavigationContainer>
      <AuthStackScreen />
    </NavigationContainer>
  )
}