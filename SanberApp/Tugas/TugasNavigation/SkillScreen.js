import React from 'react';
import { View, Image, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet, FlatList } from 'react-native';

import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';  

//data Skill
import skill_data from './skillData.json';

function Item({list}){
  return(
    <View style={styles.contentItem}>
      <MaterialCommunityIcons name={list.iconName} size={100} color="#003366" />
      <View style={styles.contentItemText}>
        <Text style={{color : '#003366', fontWeight : 'bold', fontSize : 25}}>{list.skillName}</Text>
        <Text style={{color : '#0380fc'}}>{list.categoryName}</Text>
        <Text style={{alignSelf : 'flex-end', color : 'white', fontSize : 40, fontWeight : 'bold'}}>{list.percentageProgress}</Text>
      </View>
      <MaterialCommunityIcons name="chevron-right" size={100} color="#003366" />
    </View>
  )
}

export default class SkillScreen extends React.Component {

  render(){
    return(

      <View style={styles.container}>
        <MaterialCommunityIcons name="menu" size={24} color="black"
          onPress={() => this.props.navigation.toggleDrawer()}
        />
        <View style={styles.pageHeader}>
          <Image source={require('./images/logo.png')} style={styles.logo} />
          <View style={styles.userDescContainer}>
            <MaterialIcons name="account-circle" size={50} color="black" />
            <View style={styles.userName}>
              <Text style={{fontSize : 15}}>Hai</Text>
              <Text style={{fontSize : 20, fontWeight : 'bold', color : '#003366'}}>
                Herman Iskandar</Text>
            </View>
          </View>
          <View style={styles.contentTitle}>
            <Text style={{fontSize : 30, fontWeight : 'bold', color : '#003366'}}>
              SKILL
            </Text>
          </View>
          <View style={styles.contentTab}>
            <View style={styles.contentTabItem}>
              <Text style={{color : '#003366', fontWeight : 'bold'}}>
                Library / Framework
              </Text>
            </View>
            <View style={styles.contentTabItem}>
              <Text style={{color : '#003366', fontWeight : 'bold'}}>
                Bahasa Pemrograman
              </Text>
            </View>
            <View style={styles.contentTabItem}>
              <Text style={{color : '#003366', fontWeight : 'bold'}}>
                Teknologi
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.content}>
          <FlatList 
            data={skill_data.items}
            renderItem={({item}) => (
              <Item list={item} />
            )}
            keyExtractor={item => item.id}
          />
        </View>
      </View>

    )
  }

};


const styles = StyleSheet.create({
  container : {
    flex : 1,
    padding : 20,
    backgroundColor : 'white'
  },
  pageHeader : {

  },
  logo : {
    height : 60,
    width : 200,
    alignSelf : 'flex-end'
  },
  userDescContainer : {
    flexDirection : 'row',
    paddingTop : 10
  },
  userName : {
    paddingLeft : 10
  },

  contentTitle : {
    borderBottomWidth : 3,
    borderColor : '#03e7fc'
  },
  contentTab : {
    flexDirection : 'row',
    paddingTop : 10,
    justifyContent : 'space-between'
  },
  contentTabItem : {
    backgroundColor : '#03e7fc',
    alignItems : 'center',
    justifyContent : 'center',
    padding : 5,
    borderRadius : 5
  },

  content : {
    flex : 1,
    paddingTop : 10
  },
  contentItem : {
    flexDirection : 'row',
    backgroundColor : '#03e7fc',
    marginTop : 10,
    padding : 5,
    alignItems : 'center',
    justifyContent : 'space-between'
  },
  contentItemText : {
    width : 200,
    padding : 5
  }

});