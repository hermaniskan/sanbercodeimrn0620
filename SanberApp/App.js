import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

//Tugas12
import YoutubeUI from './Tugas/Tugas12/App';

//Tugas13
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';

//Tugas 14
import MainToDoScreen from './Tugas/Tugas14/components/Main';
import SkillScreen from './Tugas/Tugas14/SkillScreen';

//Tugas 15
import NavTrain from './Tugas/Tugas15/index';
import NavRoot from './Tugas/TugasNavigation/Index';

//Quiz 3
import Quiz3 from './Quiz3/index';

export default function App() {
  return (
    <Quiz3 />
  );
}

const styles = StyleSheet.create({

});
