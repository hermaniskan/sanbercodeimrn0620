// No.1 - Looping While

console.log("LOOPING PERTAMA");
var base = 2;
while(base <= 20){
    console.log(base + " - I Love Coding");
    base += 2;
}

console.log("LOOOPING KEDUA");
var base2 = 20;
while(base2 >= 2){
    console.log(base2 + " - I Wil become a Mobile Developer");
    base2 -= 2;
}



// No.2 - Looping For
console.log("OUTPUT");
for(var base = 1; base <= 20; base ++){
    if(base % 2 == 1 && base % 3 != 0){
        console.log(base + " - Santai");
    }else if(base % 2 == 1 && base % 3 == 0){
        console.log(base + " - I Love Coding");
    }else if(base % 2 == 0){
        console.log(base + " - Berkualitas");
    }
}



// No.3 -  Membuat persegi panjang
console.log("----- PERSEGI PANJANG -----");
for(var base = 1; base <= 4; base++){
    var output = "";
    for(var base2 = 1; base2 <= 8; base2++){
        output +=  "#";
    }
    console.log(output);
}



// No.4 - Membuat Tangga
console.log("----- TANGGA -----");
for(var base = 1; base <= 7; base++){
    var output = "";
    for(var base2 = 1; base2 <= base; base2++){
        output += "#";
    }
    console.log(output);
}



// No.5 Membuat Papan Catur
console.log("----- PAPAN CATUR -----");
for(var base = 1; base <= 8; base++){
    var output = "";
    for(var base2 = 1; base2 <= 4; base2++){
        if(base % 2 == 1){
            output += " #";
        }else{
            output += "# ";
        }
    }
    console.log(output);
}