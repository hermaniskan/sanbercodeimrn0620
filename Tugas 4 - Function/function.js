//No.1

function teriak(){
    var hello = "Hello Sanbers !"
    return hello;
}

console.log(teriak());



//No.2

function kalikan(angka1, angka2){
    var hasil = angka1 * angka2;

    return hasil;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);



//No.3

function introduce(name, age, address, hobby){
    var intro = "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!";

    return intro;
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Jogjakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);