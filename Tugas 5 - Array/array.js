//No.1 (Range)
function range(startNum, finishNum){
    var arr = [];
    if(startNum < finishNum){
        for(var i = startNum; i <= finishNum; i++){
            arr.push(i)
        }
        return arr;
    }else if(startNum > finishNum){
        for(var i = startNum; i >= finishNum; i--){
            arr.push(i)
        }
        return arr;
    }else{
        return -1;
    }
}
console.log(range(1, 10)); 
console.log(range(1)); 
console.log(range(11,18)); 
console.log(range(54, 50));
console.log(range());
console.log("----------------------------------------------");



//No.2 (Range with Step)
function rangeWithStep(startNum, finishNum, step){
    var arr = [];
    if(startNum < finishNum){
        for(var i = startNum; i <= finishNum; i+=step){
            arr.push(i)
        }
    }else if(startNum > finishNum){
        for(var i = startNum; i >= finishNum; i-=step){
            arr.push(i)
        }
    }

    return arr;
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3)); 
console.log(rangeWithStep(5, 2, 1)); 
console.log(rangeWithStep(29, 2, 4));
console.log("----------------------------------------------");



//No.3 (Sum of Range)
function sum(num1=0, num2=0, step = 1){
    var sum = 0;
    var arr = rangeWithStep(num1,num2,step);
    for(var i = 0;  i < arr.length; i++){
        sum += arr[i]
    }

    return sum;
}
console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());
console.log("----------------------------------------------");



//No.4 (Array Multidimensi)
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling(input){
    for(var i = 0; i < input.length; i++){
        console.log("Nomor ID     : " + input[i][0]);
        console.log("Nama Lengkap : " + input[i][1]);
        console.log("TTL          : " + input[i][2] + ", " + input[i][3]);
        console.log("Hobby        : " + input[i][4]);
        console.log("******************************************")
    }
}

dataHandling(input);
console.log("----------------------------------------------");



//No.5 (Balik Kata)
function balikKata(kata){
    var kataBalik = "";
    for(i = kata.length-1; i >= 0; i--){
        kataBalik += kata[i];
    }

    return kataBalik;
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers")) 
console.log("----------------------------------------------");



//No.6 (Metode Array)
function dataHandling2(arr){
    arr.splice(1, 1, "Roman Alamsyah Elsharawy")
    arr.splice(2, 1, "Provinsi Bandar Lampung")
    arr.splice(4, 1, "Pria")
    arr.splice(5, 0, "SMA Internasional Metro")
    console.log(arr);
    var newDate = arr[3].split("/");
    var newDate2 = arr[3].split("/");
    switch(newDate[1]){
        case '01': { console.log(' Januari '); break; }
        case '02': { console.log(' Februari '); break; }
        case '03': { console.log(' Maret '); break; }
        case '04': { console.log(' April '); break; }
        case '05': { console.log(' Mei '); break; }
        case '06': { console.log(' Juni '); break; }
        case '07': { console.log(' Juli '); break; }
        case '08': { console.log(' Agustus '); break; }
        case '09': { console.log(' September '); break; }
        case '10': { console.log(' Oktober '); break; }
        case '11': { console.log(' November '); break; }
        case '12': { console.log(' Desember '); break; }
    }

    var tempDate = []
    for(i = 0; i < 3; i++){
        tempDate.push(parseInt(newDate[i]))
    }
    newDate = tempDate;
    var newDateDesc = newDate.sort(function (value1, value2) { return value1 < value2 } )
    console.log(newDateDesc);
    var newDateReformat = newDate2.join("-");
    console.log(newDateReformat);
    var newName = arr[1].slice(0,15)
    console.log(newName);
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);